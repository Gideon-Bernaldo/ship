using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public int CountLevels = 3;

    [Header("Windows")]
    public GameObject MenuWindow;
    public GameObject LevelWindow;

    [Header("Audio")]
    public AudioSource SoundSource;
    [Space(5)]
    public AudioClip EnterButtonClip;
    public AudioClip PressButtonClip;
    [Header("Text")]
    public Text[] TextLevel;
    public List<WaveData> levels;

    private void Awake()
    {
        for (int i = 1; i <= CountLevels; i++)
        {
            levels.Add((WaveData)Resources.Load($"Level/Wave{i}"));
        }
    }

    private void Start()
    {
        for (int i = 1; i <= CountLevels; i++)
        {            
            TextLevel[i - 1].text = PlayerPrefs.HasKey($"Ship{i}")
                ?  $"{levels[i - 1].Wave_Setting.CountShips - PlayerPrefs.GetInt($"Ship{i}")}/{levels[i - 1].Wave_Setting.CountShips}"
                : $"0/{levels[i - 1].Wave_Setting.CountShips}";
        }
    }

    public void ButtonPlay()
    {
        MenuWindow.SetActive(false);
        LevelWindow.SetActive(true);

        PressButton();
    }

    public void ButtonBack()
    {
        MenuWindow.SetActive(true);
        LevelWindow.SetActive(false);

        PressButton();
    }

    public void ButtonQuit()
    {
        Application.Quit();
    }

    public void EnterButton()
    {
        SoundSource.PlayOneShot(EnterButtonClip);
    }

    public void DeleteSaves()
    {
        for (int i = 1; i <= CountLevels; i++)
        {
            TextLevel[i - 1].text = PlayerPrefs.HasKey($"Ship{i}")
                ? $"{levels[i - 1].Wave_Setting.CountShips - PlayerPrefs.GetInt($"Ship{i}")}/{levels[i - 1].Wave_Setting.CountShips}"
                : $"0/{levels[i - 1].Wave_Setting.CountShips}";
        }

        PlayerPrefs.DeleteAll();
    }
    
    void PressButton()
    {
        SoundSource.PlayOneShot(PressButtonClip);
    }
}
