using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour
{
    public void ClickOnLoad (int level)
    {
        GetLevel.NumLevel = level;
        SceneManager.LoadScene(1);
    }
}
