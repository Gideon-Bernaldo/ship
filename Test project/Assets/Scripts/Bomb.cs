using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public GameObject BoomEffect;

    private void Awake()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 direction = mousePos - transform.position;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, direction);
    }

    private void LateUpdate()
    {
        transform.Translate(Vector3.up * 5 * Time.deltaTime);
        Destroy(gameObject, 5);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Ship"))
        {
            GameController.Instance.countShipsSave--;
            GameController.Instance.EnterTextShip();
            PlayerPrefs.SetInt($"Ship{GameController.Instance.Level}", GameController.Instance.countShipsSave);
            Instantiate(BoomEffect, collision.transform.position, Quaternion.identity);
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }

}
