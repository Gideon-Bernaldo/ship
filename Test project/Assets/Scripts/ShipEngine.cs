using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipEngine : MonoBehaviour
{
    public static ShipEngine Instance;

    Transform camPos;
    Vector3 rightLine = new Vector3(.5f, .5f, .5f);
    Vector3 leftLine = new Vector3(-.5f, .5f, .5f);        
    Vector2 startPos;

    public float endPosX;
    public int direction = 0;

    int speed;    

    private void Awake()
    {        
        startPos = transform.position;
        camPos = Camera.main.transform;
        if (transform.position.x - camPos.position.x < 0)
        {
            transform.localScale = leftLine;
            direction++;
        }
        else
        {
            transform.localScale = rightLine;
            direction--;
        }
        speed = Random.Range(2, 4);

        Instance = this;
    }

    private void LateUpdate()
    {
        transform.Translate(transform.right * direction * speed * Time.deltaTime);
        

        if (transform.localScale == leftLine)
        {
            if (transform.position.x >= endPosX) transform.position = startPos;
        }
        else
        {
            if (transform.position.x <= endPosX) transform.position = startPos;
        }
    }   
}
