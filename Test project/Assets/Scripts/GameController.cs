using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public bool ViewDetails = false;
    public int Level;
    public int MaxLevels = 3;
    [Space(10)]
    public Text TextShip;
    public Text TextBomb;
    public GameObject EndWindow;
    public GameObject ButtonNext, ButtonRestart;
    public Image BackImgEnd;    

    /*[HideInInspector]*/ public int countShipsSave, countBombsSave;
    [HideInInspector] public float AddX = 110f, AddY = -55f;
    float timer = 0;
    public int countShipsNow, countShipSpawn;
    bool stopGame = false;

    Camera camera;
    Vector3[] points = new Vector3[4];
    GameObject Bomb;
    public WaveData Options;
    public WaveSettings Wave
    {
        get { return Options.Wave_Setting; }
        protected set
        { }
    }
    public static GameController Instance;

    private void Awake()
    {
        Instance = this;
        camera = Camera.main;
        Level = GetLevel.NumLevel;
        try
        {
            Options = (WaveData)Resources.Load($"Level/Wave{Level}");
            Bomb = (GameObject)Resources.Load("Level/Bomb/Bomb");
        }
        catch
        {
            Debug.Log("������ �������� ������ ����� ������. ������ SpawnShips, ������ 23");
        }        

        countBombsSave = PlayerPrefs.HasKey($"Bomb{Level}") ? PlayerPrefs.GetInt($"Bomb{Level}") : Wave.CountBombs;
        //countShipsSave = PlayerPrefs.HasKey($"Ship{Level}") ? PlayerPrefs.GetInt($"Ship{Level}") : Wave.CountShips;

        if (PlayerPrefs.HasKey($"Ship{Level}"))
        {
            countShipsSave = PlayerPrefs.GetInt($"Ship{Level}");
            countShipSpawn = countShipsSave;
        }
        else
        {
            countShipsSave = Wave.CountShips;
            countShipSpawn = countShipsSave;
        }


        points[0] = camera.ScreenToWorldPoint(new Vector2(0f - AddX, 0f - AddY));
        points[1] = camera.ScreenToWorldPoint(new Vector2(0f - AddX, camera.pixelHeight + AddY));
        points[2] = camera.ScreenToWorldPoint(new Vector2(camera.pixelWidth + AddX, camera.pixelHeight + AddY));
        points[3] = camera.ScreenToWorldPoint(new Vector2(camera.pixelWidth + AddX, 0f - AddY));

        EnterTextShip();
        EnterTextBomb();
    }

    private void LateUpdate()
    {
        if (!stopGame)
        {
            if (countShipsNow < countShipSpawn)
            {
                if (timer > 0) timer -= Time.deltaTime;
                else SpawnShips();
            }

            SpawnBomb();
        }

        if (countBombsSave == 0 && countShipsSave > 0) OpenEndWindow(false);
        else if (countBombsSave >= 0 && countShipsSave == 0) OpenEndWindow(true);
    }

    public void SpawnShips()
    {
        countShipsNow++;
        Instantiate(Wave.ObjectShips[Random.Range(0, Wave.ObjectShips.Length)], GetSpawnPosition(Wave.SpawnPositions), Quaternion.identity);

        ShipEngine shipEngine = ShipEngine.Instance;
        shipEngine.endPosX = shipEngine.direction == 1 ? shipEngine.endPosX = points[2].x : shipEngine.endPosX = points[1].x;

        timer = Wave.SpawnTime;
    }

    public void SpawnBomb()
    {
        if (Input.GetMouseButtonDown(0) && Wave.CountBombs > 0)
        {            
            countBombsSave--;
            EnterTextBomb();
            Instantiate(Bomb, camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth / 2, 40f, 0)), Quaternion.identity);
            PlayerPrefs.SetInt($"Bomb{Level}", countBombsSave);
        }
    }

    public Vector3 GetSpawnPosition(int spawnPosition)
    {
        Vector3 vec;

        switch (spawnPosition)
        {
            case 0:
                vec = new Vector3(Random.Range(points[0].x, points[1].x), Random.Range(points[0].y, points[1].y), 0f);
                break;
            case 1:
                vec = new Vector3(Random.Range(points[2].x, points[3].x), Random.Range(points[2].y, points[3].y), 0f);
                break;
            case 2:
                float i = Random.Range(0f, 1f);
                vec = i <= .5f ?
                    new Vector3(Random.Range(points[0].x, points[1].x), Random.Range(points[0].y, points[1].y), 0f) :
                    new Vector3(Random.Range(points[2].x, points[3].x), Random.Range(points[2].y, points[3].y), 0f);
                break;

            default:
                vec = new Vector3(Random.Range(points[0].x, points[1].x), Random.Range(points[0].y, points[1].y), 0f);
                break;

        }

        return vec;
    }

    public void EnterTextShip()
    {
        TextShip.text = $"{Wave.CountShips - countShipsSave}/{Wave.CountShips}";
    }

    public void EnterTextBomb()
    {
        TextBomb.text = $"{Wave.CountBombs - countBombsSave}/{Wave.CountBombs}";
    }

    public void OpenEndWindow(bool win)
    {
        ButtonNext.SetActive(win);
        ButtonRestart.SetActive(!win);

        stopGame = true;
        EndWindow.SetActive(true);
        BackImgEnd.color = win
            ? new Color(0, 1, 0, .3f)
            : new Color(1, 0, 0, .3f);
    }

    public void NextLevel()
    {
        if(Level + 1 <= MaxLevels) GetLevel.NumLevel = Level + 1;
        else GetLevel.NumLevel = 1;
        SceneManager.LoadScene(1);
    }

    public void Restart()
    {
        PlayerPrefs.DeleteKey($"Ship{Level}");
        PlayerPrefs.DeleteKey($"Bomb{Level}");

        SceneManager.LoadScene(1);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }

    private void OnDrawGizmos()
    {
        if (ViewDetails)
        {
            Vector3 p1 = Camera.main.ScreenToWorldPoint(new Vector2(0f - AddX, 0f - AddY));
            Vector3 p2 = Camera.main.ScreenToWorldPoint(new Vector2(0f - AddX, Camera.main.pixelHeight + AddY));
            Vector3 p3 = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth + AddX, Camera.main.pixelHeight + AddY));
            Vector3 p4 = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth + AddX, 0f - AddY));

            Gizmos.color = Color.green;
            Gizmos.DrawLine(p1, p2); // Left wall
            Gizmos.color = Color.green;
            Gizmos.DrawLine(p3, p4); // Right wall
        }                
    }
}
