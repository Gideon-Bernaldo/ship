using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class WaveSettings
{
    public int CountShips;
    public int CountBombs;
    public float SpawnTime;
    [Range(0, 2)]
    public int SpawnPositions = 0;

    public GameObject[] ObjectShips;
}

[CreateAssetMenu(menuName = "Databases/Wave", fileName = "Wave")]
public class WaveData : ScriptableObject
{
    public WaveSettings Wave_Setting;
}
